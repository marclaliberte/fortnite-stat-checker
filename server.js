const express = require('express')
const bodyParser = require('body-parser');
var path = require("path");
const Fortnite = require('fortnite-api');
const opn = require('opn');
var fs = require("fs");

const app = express()

let fortniteAPI = new Fortnite(["laliberte.marc1@gmail.com", "3>jyB5QODu2[67ZspNB", "MzRhMDJjZjhmNDQxNGUyOWIxNTkyMTg3NmRhMzZmOWE6ZGFhZmJjY2M3Mzc3NDUwMzlkZmZlNTNkOTRmYzc2Y2Y=", "ZWM2ODRiOGM2ODdmNDc5ZmFkZWEzY2IyYWQ4M2Y1YzY6ZTFmMzFjMjExZjI4NDEzMTg2MjYyZDM3YTEzZmM4NGQ="]);
var body=[];


app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json('application/json'));

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname+'/index.html'))
	
})


app.post('/serverStatus', function (req, res) {
	fortniteAPI.login().then(() => {
		fortniteAPI
		.checkFortniteStatus()
		.then(status => {
			res.send(status);
		})
		.catch(err => {
			console.log(err);
		});
	});
})

app.post('/getStore', function (req, res) {
	fortniteAPI.login().then(() => {
		fortniteAPI
		.getStore("en")
		.then(store => {
			res.send(store)
		})
		.catch(err => {
			console.log(err);
		});
	});
})

app.get('/store.html', function (req, res) {
	res.sendFile(path.join(__dirname+'/store.html'))
})
app.get('/landerHelper.html', function (req, res) {
	res.sendFile(path.join(__dirname+'/landerHelper.html'))
})

app.get('/index.html', function (req, res) {
	res.sendFile(path.join(__dirname+'/index.html'))
})

app.get('/news.html', function (req, res) {
	res.sendFile(path.join(__dirname+'/news.html'))
})

app.post('/getNews', function (req, res) {
	fortniteAPI.login().then(() => {
		fortniteAPI
		.getFortniteNews("en")
		.then(news => {
			res.send(news);
		})
		.catch(err => {
			console.log(err);
		});
	});
})


app.post('/stats', function (req, res) {
	var name = req.body.name;
	var plat = req.body.platform;
	//console.log("Querying Epic servers with name : ",name,"and plat: ", plat)
	fortniteAPI.login()
	.then(() => 
	{
		fortniteAPI.getStatsBR(name, plat)
		.then((stats) => 
		{		  	     
			res.send(stats);
		})
		.catch((err) => 
		{
			//null response if profile not found
			res.send("");
		});
	});
})


app.listen(8888,() => console.log('Now listening to 8888'));
opn('http://localhost:8888', {app: 'chrome'});
