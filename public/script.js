var searchHistory =[];


window.onload=function(){
    generateEmptyPage();
    if($(window).width()>1400){
        $('div.separateur').html('<br>')
    }

    $('#userName').keypress(function (e) {
       var key = e.which;
        if(key == 13 && $("#userName").val()!=''){  
            stats();                    
        }
    });
    //$('div.serverStatus').html()
     $.ajax({
        type: 'POST',
        url: '/serverStatus',
        success: function(result) {
          var string = "Fortnite servers are currently "

          if(result==true){
            $('div.serverStatus').html(string+="online.")
          }else{
            $('div.serverStatus').html(string+="offline.")
          }
      },
  });

}


function launchRequest(name,plat){
    $.ajax({
        type: 'POST',
        url: '/stats',
        data: {"name":name,"platform":plat},
        success: function(result) {
          parseResult(result);
      },
  });
}

//called with search
function stats(){
    var plat = $('#platform').val();
    var name = $('#userName').val();
    launchRequest(name,plat);
    
}


function generateEmptyPage(){
    $('div.soloRow').html(createEmptyTable(1));
    $('div.duoRow').html(createEmptyTable(2))
    $('div.squadRow').html(createEmptyTable(3))
    $('div.lifeRow').html(createEmptyTable(4))    
}

function nullProfile(){
    generateEmptyPage();
    $('#accountName').text("User does not exist");
}



//generate page with result from search
function parseResult(result){   
    if(result==''){
      nullProfile();
      return;
  }
  putInHistory(result);
  var stats=result;
    //console.log(stats);
    $('#userName').val("");
    $('#accountName').html(stats.info.username+" - <span id='platResult'>"+stats.info.platform+"</span>");
    $('div.soloRow').html(createTable(stats.group.solo,1))
    $('div.duoRow').html(createTable(stats.group.duo,2))
    $('div.squadRow').html(createTable(stats.group.squad,3))
    $('div.lifeRow').html(createTable(stats.lifetimeStats,4))
}

function putInHistory(result){
    console.log(result);
    var stats=result;
    var stringName=stats.info.username+"";
    var stringPlat = stats.info.platform+"";
    var link="<li><button type='button' onclick='launchRequest(\""+stringName+"\",\""+stringPlat+"\")'>"+stats.info.username+" <span id=platResult>"+stats.info.platform+"</span></button></li>"
    for(var i=0;i<searchHistory.length;i++){
        if(searchHistory[i]===link){
            return;
        }       
    }   
    searchHistory.push(link); 
    updateHistory();
}


function updateHistory(){
    var result='<ul><p id="historyTitle">Search History</p>';
    for(var i=0;i<searchHistory.length;i++){
        result+=searchHistory[i];
    }
    result+='</ul>'
    $('div.searchHistory').html(result);
}


function generateTh(index){
    if(index==1){
        return "<tr><th>Score</th><th>Kills</th><th>Wins</th><th>K / D</th><th>Top 10</th><th>Top 25</th><th>Matches</th><th>Win%</th><th>Score/match</th></tr>";
    }
    else if(index==2){
        return "<tr><th>Score</th><th>Kills</th><th>Wins</th><th>K / D</th><th>Top 5</th><th>Top 12</th><th>Matches</th><th>Win%</th><th>Score/match</th></tr>";
    }
    else if(index==3){
        return "<tr><th>Score</th><th>Kills</th><th>Wins</th><th>K / D</th><th>Top 3</th><th>Top 6</th><th>Matches</th><th>Win%</th><th>Score/match</th></tr>"
    }
    else{
        return "<tr><th>Score</th><th>Kills</th><th>Wins</th><th>K / D</th><th>Top 3</th><th>Top 5</th><th>Top 6</th><th>Top 10</th><th>Top 12</th><th>Top 25</th><th>Matches</th><th>Win%</th><th>Score/match</th></tr>"
    }

}



function createEmptyTable(index){
    var title='';
    if(index==1){
        title="Solo   "
    }else if(index==2){
        title="Duo   "
    }else if(index==3){
        title="Squad "
    }else{
        title='LifeTime'
    }
    var result='<table id=table'+index+'>'
    result+='<caption>'+title+'</caption>'
    if(index==1){
        result+=generateTh(index);
        result+="<tr><td>"+0
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0
        +"</td><td>"+0
        +'</td><td>'+0                            
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0+"</td></tr>";
        return result;
    }
    if(index==2){
        result+=generateTh(index);
        result+="<tr><td>"+0
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0
        +"</td><td>"+0
        +'</td><td>'+0                               
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0+"</td></tr>";
        return result;
    }


    if(index==3){
        result+=generateTh(index);
        result+="<tr><td>"+0
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0
        +"</td><td>"+0
        +'</td><td>'+0              
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0+"</td></tr>";
        return result;
    }
    if(index==4){
        result+=generateTh(index);
        result+="<tr><td>"+0
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0
        +"</td><td>"+0
        +'</td><td>'+0
        +"</td><td>"+0
        +"</td><td>"+0
        +'</td><td>'+0
        +'</td><td>'+0                  
        +'</td><td>'+0
        +'</td><td>'+0
        +'</td><td>'+0+"</td></tr>"

        return result;
    }
}


function createTable(object,index){
   var title='';
   if(index==1){
    title="Solo   "
}else if(index==2){
    title="Duo   "
}else if(index==3){
    title="Squad "
}else{
    title='LifeTime '
}
var result='<table id=table'+index+'>'
result+='<caption>'+title+'</caption>'
if(index==1){
    result+=generateTh(index);
    result+="<tr><td>"+object['score']
    +'</td><td>'+object['kills']
    +'</td><td>'+object['wins']
    +'</td><td>'+object['k/d']  
    +"</td><td>"+object['top10']
    +'</td><td>'+object['top25']                           
    +'</td><td>'+object['matches']
    +'</td><td>'+object['win%']
    +'</td><td>'+Math.round(object['score']/object['matches'])+"</td></tr>";
    return result;
}
if(index==2){
    result+=generateTh(index);
    result+="<tr><td>"+object['score']
    +'</td><td>'+object['kills']
    +'</td><td>'+object['wins']
    +'</td><td>'+object['k/d']
    +"</td><td>"+object['top5']
    +'</td><td>'+object['top12']              
    +'</td><td>'+object['matches']
    +'</td><td>'+object['win%']
    +'</td><td>'+Math.round(object['score']/object['matches'])+"</td></tr>";
    return result;
}


if(index==3){
    result+=generateTh(index);
    result+="<tr><td>"+object['score']
    +'</td><td>'+object['kills']
    +'</td><td>'+object['wins']
    +'</td><td>'+object['k/d']
    +"</td><td>"+object['top3']
    +'</td><td>'+object['top6']               
    +'</td><td>'+object['matches']
    +'</td><td>'+object['win%']
    +'</td><td>'+Math.round(object['score']/object['matches'])+"</td></tr>";
    return result;
}
if(index==4){
    result+=generateTh(index);
    result+="<tr><td>"+object['score']
    +'</td><td>'+object['kills']
    +'</td><td>'+object['wins']
    +'</td><td>'+object['k/d']
    +"</td><td>"+object['top3s']
    +'</td><td>'+object['top5s']           
    +"</td><td>"+object['top6s']
    +"</td><td>"+object['top10s']
    +'</td><td>'+object['top12s']
    +'</td><td>'+object['top25s']               
    +'</td><td>'+object['matches']
    +'</td><td>'+object['win%']
    +'</td><td>'+Math.round(object['score']/object['matches'])+"</td></tr>";

    return result;
}

}

