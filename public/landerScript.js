var landingZones=[
    ['Junk Junction', 136, 100],
    ['Haunted Hills', 101, 172],
    ['Pleasant Park', 208, 235],
    ['Snobby Shores', 48, 364],
    ['Motel', 303,143],
    ['Anarchy Acres', 404,165],
    ['Tilted Towers', 290,396],
    ['Dusty Divot', 464,409],
    ['Tomato Town', 519,263],
    ['Retail Row', 582,436],
    ['Spot a Marc', 642,506],
    ['Salty Springs', 443,494],
    ['Shifty Shafts', 277,516],
    ['Greasy Grove', 165,498],
    ['Fatal Fields', 469,607],
    ['Prison', 598,601],
    ['Moisty Mire', 657,632],
    ['Waling Woods', 651,246],
    ['Lucky Landing', 427,749],
    ['Flush Factory', 274,708],
    ['Risky Reels', 579,161],
    ['Manor', 745,419]
]

function getRandomfromArray(array){
    return array[Math.floor(Math.random()*array.length)]
}


function landerGenerator(){
    var zone =getRandomfromArray(landingZones);
    $('div.landingZone').text(zone[0]);
    var c=document.getElementById("mapCanvas");
    var ctx=c.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    ctx.arc(zone[1],zone[2],10,0,2*Math.PI);
    ctx.lineWidth=3;
    ctx.strokeStyle="red";
    ctx.stroke();
}

var canvas = document.getElementById('mapCanvas');
function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
      }

canvas.addEventListener('mousemove', function(evt) {
        var mousePos = getMousePos(canvas, evt);
        console.log(Math.round(mousePos.x) + ',' + Math.round(mousePos.y));
        //writeMessage(canvas, message);
      }, false)
