$(document).ready(function() {
	$.ajax({
		type: 'POST',
		url: '/getNews',
		success: function(result) {
			//console.log(result.br);
			parseResult(result.br);
		},
	});
})


function parseResult(array){
	var result=""
	for(var i=0;i<array.length;i++){
		result+="<div class='newsBody'>"	
		result+="<img src='"+array[i].image+"' class='newsImage'>"	
		result+="<h2 class='newsHeader'>"+array[i].title+"</h2>"		
		result+="<p class='newsText'>"+array[i].body+"</p>"
		result+="</div>"
	}
	$('.newsWrapper').html(result);
}